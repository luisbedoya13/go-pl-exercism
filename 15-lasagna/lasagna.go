package lasagna

func PreparationTime(layers []string, averageTime int) int {
	if averageTime == 0 {
		averageTime = 2
	}
	return len(layers) * averageTime
}

func Quantities(layers []string) (noodles int, sauce float64) {
	numLayers := len(layers)
	for i := 0; i < numLayers; i++ {
		if layers[i] == "noodles" {
			noodles += 50
		}

		if layers[i] == "sauce" {
			sauce += 0.2
		}
	}
	return
}

func AddSecretIngredient(friendsList, myList []string) {
	myList[len(myList)-1] = friendsList[len(friendsList)-1]
}

func ScaleRecipe(list []float64, portions int) []float64 {
	output := make([]float64, len(list))
	for i := 0; i < len(list); i++ {
		output[i] = list[i] * float64(portions) / 2
	}
	return output
}
