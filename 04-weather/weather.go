// Package weather helps with weather forecast in Goblinocus.
package weather

// CurrentCondition stores current weather condition.
var CurrentCondition string
// CurrentLocation stores current location.
var CurrentLocation string

// Forecast returns the weather forecast for a city in Goblinocus.
func Forecast(city, condition string) string {
	CurrentLocation, CurrentCondition = city, condition
	return CurrentLocation + " - current weather condition: " + CurrentCondition
}